from shutil import rmtree
from unittest import TestCase

from ewconfig.pz.xml import Response
from ewconfig.lib.filter import ChannelFilter
from tests.lib.log import LogMixin
from tests.lib.x2ew import MainMixin


class TestBadUnits(MainMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_station_xml(self):
        dest_dir = self.run_xml2ew('BAD-UNITS', chan_filter=ChannelFilter(exclude='LOG'),
                                   lo_precision=True)
        with self.assertRaises(AssertionError):
            self.assert_files(dest_dir, 'BAD-UNITS')

    def test_station_xml_with_default(self):
        dest_dir = self.run_xml2ew('BAD-UNITS',
                                   chan_filter=ChannelFilter(exclude='LOG'), lo_precision=True,
                                   default_response=Response.ACCELERATION)
        self.assert_files(dest_dir, 'BAD-UNITS')
        rmtree(dest_dir)
