from unittest import TestCase

from ewconfig.lib.filter import GlobFilter, DottedFilter
from tests.lib.log import LogMixin


class TestFilter(LogMixin, TestCase):

    stderr_verbosity = 5

    def test_literal(self):
        self.assertFilter('a b', 'b,c', ['a', 'b', 'c'], ['a'])

    def test_default(self):
        self.assertFilter('', 'b c', ['a', 'b', 'c'], ['a'])

    def test_pattern(self):
        self.assertFilter('?x*', '*z', ['a', 'x', 'xx', 'xxxx', 'xxxz'], ['xx', 'xxxx'])

    def assertFilter(self, include, exclude, all, target):
        filter = GlobFilter('test', prefix=False, include=include, exclude=exclude)
        result = list(filter(all))
        self.assertEqual(target, result)


class TestDottedFilter(LogMixin, TestCase):

    stderr_verbosity = 5

    def test_literal(self):
        self.assertFilter('a, b', 'b c', ['a', 'b', 'c'], ['a'])

    def test_default(self):
        self.assertFilter('', 'b,c', ['a', 'b', 'c'], [])

    def test_pattern(self):
        self.assertFilter('?x*', '*z', ['a', 'x', 'xx', 'xxxx', 'xxxz'], ['xx', 'xxxx'])

    def test_dotted(self):
        self.assertFilter('*.*.hh[ne].*', '', ['a.b.hhn.00', 'a.b.hhn'], ['a.b.hhn.00'])

    def assertFilter(self, include, exclude, all, target):
        filter = DottedFilter('test', include, exclude)
        result = list(filter(all))
        self.assertEqual(target, result)

