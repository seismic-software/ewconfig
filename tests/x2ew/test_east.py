from shutil import rmtree
from unittest import TestCase

from ewconfig.lib.filter import GlobFilter
from tests.lib.log import LogMixin
from tests.lib.x2ew import MainMixin


class TestEast(MainMixin, LogMixin, TestCase):
    """
    this uses an "inverted" CI-PASC with station in the S/E to check that hinv_sta.d
    includes appropriate characters.
    """

    stderr_verbosity = 5

    def test_station_xml(self):
        dest_dir = self.run_xml2ew('east',
                                   chan_filter=GlobFilter('Channel', prefix=True, exclude='LOG'),
                                   lo_precision=True)
        # ignore pressure sensitive channels which are now excluded
        self.assert_files(dest_dir, 'east')
        rmtree(dest_dir)
