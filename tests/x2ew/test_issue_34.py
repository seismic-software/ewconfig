from shutil import rmtree
from unittest import TestCase

from tests.lib.log import LogMixin
from tests.lib.x2ew import MainMixin


class TestIssue34(MainMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_station_xml(self):
        dest_dir = self.run_xml2ew('issue-34')
        self.assert_files(dest_dir, 'issue-34')
        rmtree(dest_dir)
