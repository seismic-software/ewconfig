from os import rmdir
from shutil import rmtree
from unittest import TestCase

from ewconfig.lib.file import tmp_dir
from ewconfig.lib.filter import DottedFilter
from tests.lib.log import LogMixin
from tests.lib.prune import PruneMixin


class TestCiPasc(PruneMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_original(self):
        pruned = tmp_dir()
        rmdir(pruned)  # avoid error (tests for no overwrite)
        self.run_prune('CI-PASC', filter=DottedFilter('Pattern', include='*.*.HNZ.*'),
                       force=True, pruned=pruned)
        self.assert_files(pruned, 'CI-PASC')
        rmtree(pruned)

    def test_inverse(self):
        pruned = tmp_dir()
        rmdir(pruned)  # avoid error (tests for no overwrite)
        self.run_prune('CI-PASC', filter=DottedFilter('Pattern', include='*.*.*.*', exclude='*.*.HNZ.*'),
                       force=True, pruned=pruned)
        self.assert_files(pruned, 'CI-PASC', subdir='inverse')
        rmtree(pruned)
