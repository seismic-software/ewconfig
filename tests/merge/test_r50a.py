from filecmp import dircmp
from logging import getLogger
from os import rmdir
from re import search
from shutil import rmtree
from unittest import TestCase

from ewconfig.lib.file import tmp_dir
from ewconfig.lib.filter import ChannelFilter
from tests.lib.log import LogMixin, CaptureLog
from tests.lib.merge import MergeMixin

log = getLogger(__name__)


class TestR50A(MergeMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_merge_ci_pasc(self):
        merged = self.run_merge('R50A', ['CI-PASC'], merged=tmp_dir(), sort='NSCL', all_chan=True)
        self.assert_files(merged, 'R50A', subdir='merge_ci_pasc', extra_dirs=('extra_dir',))
        rmtree(merged)

    def test_merge_ci_pasc_xml(self):
        # tweak various things because station xml and sacp conversion are not perfectly identical
        merged = self.run_merge('R50A', ['CI-PASC.xml'], chan_filter=ChannelFilter(exclude='LOG'),
                                lo_precision=True, merged=tmp_dir(), sort='NSCL')
        # ignore pressure sensitive channels which are now excluded
        self.assert_files(merged, 'R50A', subdir='merge_ci_pasc', extra_dirs=('extra_dir',))
        rmtree(merged)

    def test_merge_ci_pasc_xml_no_sort(self):
        # tweak various things because station xml and sacp conversion are not perfectly identical
        merged = self.run_merge('R50A', ['CI-PASC.xml'], chan_filter=ChannelFilter(exclude='LOG'),
                                lo_precision=True, merged=tmp_dir())
        # ignore pressure sensitive channels which are now excluded
        self.assert_files(merged, 'R50A', subdir='merge_ci_pasc_no_sort', extra_dirs=('extra_dir',))
        rmtree(merged)

    def test_merge_duplicates(self):
        with CaptureLog() as log:
            merged = self.run_merge('R50A', ['R50A'], lo_precision=True, merged=tmp_dir())
        # we should ONLY check the ambiguous channel
        self.assertTrue(search(r'Compared .*/N4\.R50A\.HH1\.00.sac', log.out), log.out)
        self.assertTrue(search(r'Compared .*/N4\.R50A\.HH2\.00.sac', log.out), log.out)
        self.assertTrue(search(r'Compared .*/N4\.R50A\.HHZ\.00.sac', log.out), log.out)
        self.assert_files(merged, 'R50A', subdir='merge_duplicates', extra_dirs=('extra_dir',))
        rmtree(merged)

    def test_merge_ci_pasc_forced(self):
        backup = tmp_dir()
        rmdir(backup)
        backup, old, merged = self.run_merge('R50A', ['CI-PASC'], backup=backup, sort='NSCL')
        self.assert_files(merged, 'R50A', subdir='merge_ci_pasc', extra_dirs=('extra_dir',))
        rmtree(merged)
        log.info(f'{old} v {backup}')
        cmp = dircmp(old, backup)
        cmp.report_full_closure()
        self.assertFalse(cmp.left_only)
        self.assertFalse(cmp.right_only)
        self.assertFalse(cmp.diff_files)
        self.assertFalse(cmp.funny_files)
        rmtree(backup)

    def test_merge_ci_pasc_forced_no_backup(self):
        backup, old, merged = self.run_merge('R50A', ['CI-PASC'], backup='NONE', sort='NSCL')
        self.assert_files(merged, 'R50A', subdir='merge_ci_pasc', extra_dirs=('extra_dir',))
        rmtree(merged)
        self.assertEqual(None, backup)

