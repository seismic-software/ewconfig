from logging import getLogger
from shutil import rmtree
from unittest import TestCase

from ewconfig.lib.file import tmp_dir
from ewconfig.merge.prefer import Prefer
from tests.lib.log import LogMixin
from tests.lib.merge import MergeMixin

log = getLogger(__name__)


class TestIssue33(MergeMixin, LogMixin, TestCase):

    stderr_verbosity = 5

    def test_merge_xml(self):
        merged = self.run_merge('issue-33',
                                ['OH-DSFO.xml', 'OH-HINO.xml', 'OH-KIOH.xml', 'OH-LLSO.xml', 'OH-WODO.xml'],
                                lo_precision=True, merged=tmp_dir(), prefer=Prefer.OLD, sort='NSCL')
        self.assert_files(merged, 'issue-33', subdir='merge')
        rmtree(merged)
