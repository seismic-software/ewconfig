from itertools import zip_longest
from logging import getLogger
from os import listdir
from os.path import join, exists, isdir
from re import search, sub, compile, split

from numpy.testing import assert_approx_equal

log = getLogger(__name__)


class CompareMixin:

    def _test_dir(self, name, subdir=None):
        raise NotImplementedError('_test_dir')

    def _assert_files(self, dest_dir, test_dir, rdseed=False, skip_files=None, skip_lines=None, xml=True, extra_dirs=tuple()):
        log.info(f'Comparing {test_dir} with {dest_dir}')
        skip_files_all = tuple() if rdseed else (r'rdseed\.err_log', r'SAC_PZs_.*')
        if skip_files: skip_files_all = skip_files_all + skip_files
        skip_lines_all = (r'^Date', r'^CREATED', r'^\*')
        if skip_lines: skip_lines_all = skip_lines_all + skip_lines
        subdirs = ['chan', join('eqk', 'response'), *extra_dirs]
        if xml: subdirs += ['stationxml']
        for subdir in subdirs:
            self.__assert_dirs(join(test_dir, subdir), join(dest_dir, subdir),
                               skip_files=skip_files_all)
            self.__assert_dirs(join(dest_dir, subdir), join(test_dir, subdir), contents=True,
                               skip_files=skip_files_all, skip_lines=skip_lines_all)

    def __assert_dirs(self, dir1, dir2, contents=False, skip_files=tuple(), skip_lines=tuple()):
        log.debug(f'Comparing directories {dir1} and {dir2}')
        files = set()
        if exists(dir1):
            files.update(listdir(dir1))
        if exists(dir2):
            files.update(listdir(dir2))
        for file in files:
            if not file.startswith('.'):  # skip dotted files since they are used to force git to include dirs
                if any(search(skip, file) for skip in skip_files):
                    log.debug(f'Skipping {file}')
                else:
                    path1 = join(dir1, file)
                    path2 = join(dir2, file)
                    self.assertTrue(exists(path1), f'{file} from {dir2} missing from {dir1}')
                    self.assertTrue(exists(path2), f'{file} from {dir1} missing from {dir2}')
                    if contents:
                        if not (isdir(path1) and isdir(path2)):
                            log.debug(f'Comparing file {file}')
                            with open(path1) as file1, open(path2) as file2:
                                lines1 = [line for line in file1 if not any(search(skip, line) for skip in skip_lines)]
                                lines2 = [line for line in file2 if not any(search(skip, line) for skip in skip_lines)]
                                for line1, line2 in zip_longest(lines1, lines2):
                                    if line1: line1 = self.__fix_schema_version(self.__fix_negative_zero(self.__drop_date(line1.strip())))
                                    if line2: line2 = self.__fix_schema_version(self.__fix_negative_zero(self.__drop_date(line2.strip())))
                                    self.__assert_lines(line1, line2, file, dir1, dir2)
                        elif isdir(path1) and isdir(path2):
                            self.__assert_dirs(path1, path2, contents=contents, skip_files=skip_files, skip_lines=skip_lines)
                        else:
                            raise Exception(f'{path1} and {path2} are inconsistent types (one is a directory, the other not)')

    # https://stackoverflow.com/a/55592455
    FLOAT = compile(r'[+-]?(\d+([.]\d*)?([eE][+-]?\d+)?|[.]\d+([eE][+-]?\d+)?)$')

    def __assert_lines(self, line1, line2, file, dir1, dir2):
        # log.debug(f'"{line1}" / "{line2}"')
        tokens1 = split(r'\s+', line1) if line1 else ()
        tokens2 = split(r'\s+', line2) if line2 else ()
        if len(tokens1) != len(tokens2):
            raise AssertionError(f'"{line1}" != "{line2}" in {file} ({dir1} v {dir2})')
        for (token1, token2) in zip(tokens1, tokens2):
            if self.FLOAT.match(token1) and self.FLOAT.match(token2):
                assert_approx_equal(float(token1), float(token2), significant=5,
                                    err_msg=f'{token1} != {token2} in {file} ({dir1} v {dir2})')
            else:
                self.assertEqual(token1, token2, f'"{token1}" != "{token2}" in {file} ({dir1} v {dir2})')

    def __fix_negative_zero(self, line):
        # we don't care about -0 being different to 0
        if line:
            return sub(r'\-0\.000000e\+00', r'0.000000e+00', line)
        else:
            return line

    VERSION = compile(r'(.*schemaVersion=")[\d\.]+(".*)')

    def __fix_schema_version(self, line):
        # we don't care about xml schema version
        if line:
            match = self.VERSION.match(line)
            if match:
                return match.group(1) + match.group(2)
            else:
                return line
        else:
            return line

    DATE = compile(r'(.*)(?:^|\s)\d{4}-\d{2}-\d{2}(?:$|\s)(.*)')

    def __drop_date(self, line):
        '''
        Drop dates because they can vary (this is particularly for comments with timestamps)
        (we could be more specific on the format here)
        '''
        match = self.DATE.match(line)
        if match:
            return match.group(1) + ' ' + match.group(2)
        else:
            return line

    def __compare_constants(self, line1, line2):
        # need to drop some precision here.  suspect it is ok.
        CONSTANT = compile(r'CONSTANT\s+(\S*)\s*(?:\*.*)?')  # optional group is trailing comment (units)
        constant1 = '%.4e' % float(CONSTANT.match(line1).group(1))
        constant2 = '%.4e' % float(CONSTANT.match(line2).group(1))
        self.assertEqual(constant1, constant2, f'{line1} / {line2}')
