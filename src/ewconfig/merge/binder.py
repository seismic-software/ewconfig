import os
from glob import glob
from logging import getLogger

import numpy as np
import obspy.geodetics.base as geodetics
from obspy import read_inventory
import scipy.spatial as spatial

# Import the module to avoid circular import errors.
import ewconfig.lib.write as write
from ewconfig.pz.xml import XmlPolesZeros

log = getLogger(__name__)


def polar_to_cart(lon_lat, radius = 1):
    ''' Convert lon/lat to cartesian coordinates on a sphere.

    Parameters
    ---------.
    lon_lat: :class:`np.ndarray`
        Nx2 array of longitude [:, 0] and latitude [:, 1] values.

    radius: float
       The radius of the sphere.

    Returns
    -------
    :class:`np.ndarray`
       Nx3 array of x, y, z coordinates.
    '''
    phi = lon_lat[:, 0]
    theta = 90 - lon_lat[:, 1]
    theta = np.deg2rad(theta)
    phi = np.deg2rad(phi)
    x = radius * np.sin(theta) * np.cos(phi)
    y = radius * np.sin(theta) * np.sin(phi)
    z = radius * np.cos(theta)
    return np.array(list(zip(x, y, z)))


def compute_grid_limits(lon, lat, border_win_ratio = 0.25,
                        min_border = 0.1, max_border = 10,
                        round_dec = 2):
    ''' Compute the binder grid limits from station coordinates.
    
    Parameters
    ---------.
    lon: :obj:`list` of float
        The station longitude values.
        
    lat: :obj:`list` of float
        The station latitude values.
        
    border_win_ratio: float
        The ratio of the total extent used to add a border
        to the grid (0 -1)
        
    min_border: float
        The minimum border width [degree].
        
    max_border: float
        The maximum border width [degree].

    round_dec: None or int
        Round to these number of decimals.
        If None, the values are not rounded.
        
    Returns
    -------
    :obj:`list` of float (2x2)
        The grid limits.
        [[lon_min, lon_max],
         [lat_min, lat_max]
    '''
    # Get the coordinate limits.
    min_lat = min(lat)
    max_lat = max(lat)
    min_lon = min(lon)
    max_lon = max(lon)

    # Compute the border width.
    border_lat = (max_lat - min_lat) * border_win_ratio
    border_lon = (max_lon - min_lon) * border_win_ratio

    if border_lat < min_border:
        border_lat = min_border

    if border_lat > max_border:
        border_lat = max_border

    if border_lon < min_border:
        border_lon = min_border

    if border_lon > max_border:
        border_lon = max_border

    # Add the border.
    grid_limits = [min_lon - border_lon,
                   max_lon + border_lon,
                   min_lat - border_lat,
                   max_lat + border_lat]

    # Check coordinate limits.
    if (grid_limits[0] < -180):
        grid_limits[0] = -180

    if (grid_limits[1] > 180):
        grid_limits[1] = 180

    if (grid_limits[2] < -90):
        grid_limits[2] = -90

    if (grid_limits[3] > 90):
        grid_limits[3] = 90

    if round_dec is not None:
        grid_limits = [round(x, round_dec) for x in grid_limits]

    return grid_limits


def compute_grid_parameters(lon, lat, grid_limits,
                            dspace_ratio = 4,
                            rstack_factor = 2,
                            tstack_vp = 5000,
                            tstack_ncells = 2):
    ''' Compute the binder grid parameters.

    Parameters
    ---------.
    lon: :obj:`list` of float
        The station longitude values.

    lat: :obj:`list` of float
        The station latitude values.

    grid_limits: :obj:`list` of float
        The grid limits [min_lon, max_lon, min_lat, max_lat].

    dspace_ratio: float
        The ratio used to devide the 25% quartile to get dspace.

    rstack_factor: float
        The factor used to increase the computed median of the
        minimun station distances.

    tstack_vp: float
        The p-wave velocity used to compute tstack.

    tstack_ncells: int
        The number of cells used to compute tstack.

    Returns
    -------
    :obj:`dictionary`
        The computed grid parameters with the parameter names as keys.
    '''
    ret = {}
    ret['dspace'] = 4.0
    ret['rstack'] = 100.0
    ret['tstack'] = 0.5
    stat_coords = np.array(list(zip(lon, lat)))
    stat_coords = np.unique(stat_coords, axis = 0)

    n_stations = len(stat_coords)

    # Return the default values if less than 2 stations
    # are passed.
    if n_stations <= 1:
        return ret

    # Convert lon/lat to cartesian coordinates.
    stat_cart = polar_to_cart(stat_coords)

    # Compute the grid boundary corners.
    grid_corners = [[grid_limits[0], grid_limits[2]],
                    [grid_limits[0], grid_limits[3]],
                    [grid_limits[1], grid_limits[3]],
                    [grid_limits[1], grid_limits[2]]]
    grid_corners = np.array(grid_corners)
    # Compute the grid corner cartesian coordinates on a sphere 
    grid_corners_cart = polar_to_cart(grid_corners)

    # Get station neighbors by triangulation on a sphere.
    # Add the grid corners to the station coordinates to ensure a clean convex hull.
    hull_coords = np.concatenate([stat_cart, grid_corners_cart])
    hull = spatial.ConvexHull(hull_coords)
    station_neighbors = [[] for _ in range(len(stat_coords))]
    n_stations = len(stat_coords)
    
    for cur_stat_id in range(len(stat_coords)):
        for cur_simplex in hull.simplices: #for each simplex (face; presumably a triangle) of the convex hull
            if cur_stat_id in cur_simplex:
                # Get the neighbor ids ignoring the ids of the grid corners.
                station_neighbors[cur_stat_id].extend([x for x in cur_simplex if (x != cur_stat_id) and (x < n_stations)])
        station_neighbors[cur_stat_id] = list(set(station_neighbors[cur_stat_id]))
        station_neighbors[cur_stat_id] = sorted(station_neighbors[cur_stat_id])

    # Compute the distances to the station neighbors.
    # The station distances are used to estimate suitable parameters
    # for the binder grid.
    neighbor_dist = [[] for _ in range(len(stat_coords))]
    for k in range(n_stations):
        cur_main = stat_coords[k, :]
        for m in station_neighbors[k]:
            cur_ref = stat_coords[m, :]
            cur_dist = geodetics.gps2dist_azimuth(cur_main[1], cur_main[0],
                                                  cur_ref[1], cur_ref[0])
            neighbor_dist[k].append(cur_dist[0])

    # Compute neighbor distance statistics.
    min_dist_list = []
    median_dist_list = []
    for cur_neighbor_dist in neighbor_dist:
        cur_min_dist = np.min(cur_neighbor_dist)
        cur_median_dist = np.median(cur_neighbor_dist)
        min_dist_list.append(cur_min_dist)
        median_dist_list.append(cur_median_dist)

    #log.info('#### Computing grid parameters.')
    #log.info('#### dist: {}.'.format(dist))
    #log.info('#### n_stations: {}.'.format(n_stations))
    #log.info('#### stat_coords: {}.'.format(stat_coords))
    
    # Compute the binder parameters.
    # dspace
    # The side lenght of the binder grid is computed using the minimum
    # of all inter-station distances and the given ratio.
    dspace = np.quantile(min_dist_list, 0.25) / dspace_ratio
    # Round the grid length to km, 100m, 10m or m based on the length.
    if dspace > 1000:
        dspace = np.floor(dspace / 1000) * 1000
    elif dspace > 100:
        dspace = np.floor(dspace / 100) * 100
    elif dspace > 10:
        dspace = np.floor(dspace / 10) * 10

    if dspace <= 0:
        msg = ("Computed dspace is smaller than 0 m. "
               "This seems to be a very small network "
               "(min. station distances: {}). Reset dspace "
               "to 1 m.").format(min_dist_list)
        log.warning(msg)
        dspace = 1

    # rstack
    # rstack defines the distance over which phases are allowed to interact
    # with other phases. Compute it based on the median neighbor distance.
    med_dist = np.median(min_dist_list)
    rstack = med_dist * rstack_factor
    # Make rstack a multiple of dspace.
    rstack = rstack - (rstack % dspace)

    # tstack
    # As a first approximation, tstack should be at least the number
    # of seconds it takes for a P-wave to cross a grid cell of dspace km.
    # Compute the traveltime using a constant vp velocity.
    ncells = tstack_ncells
    vp = tstack_vp
    tstack = (dspace * ncells) / vp
    if tstack < 0.01:
        tstack = 0.01

    # Convert to km.
    dspace_km = dspace / 1000
    rstack_km = rstack / 1000

    # Round the parameters.
    # TODO: Round the grid parameters depending on the station layout
    #       (e.g. min station distance, median station distance, ...).
    dspace_km = np.round(dspace_km, 3)
    rstack_km = np.round(rstack_km, 3)
    tstack = np.round(tstack, 2)

    ret = {}
    ret['dspace'] = dspace_km
    ret['rstack'] = rstack_km
    ret['tstack'] = tstack

    return ret

# TODO: Split compute_binder_grid into functions to load the inventory
# from a directory and to compute the grid using the loaded inventory.
def compute_binder_grid(base_dir, subdir='stationxml'):
    ''' Compute the binder grid using stationXML files.
    '''
    def xml_files_in(dir):
        search_path = os.path.join(dir, '**', '*.xml')
        yield from (file for file in glob(search_path, recursive = True))
 
    xml_path = os.path.join(base_dir, subdir)

    # Read all available station XML files of the merged directory.
    inv = None
    for file in xml_files_in(xml_path):
        cur_inv = read_inventory(file)
        if inv is None:
            inv = cur_inv
        else:
            inv.extend(cur_inv)

    if not inv:
        log.warning(f'No station XML files in {xml_path}')
    else:
        stat_coords = []
        # Compute the coordinates of each station as the mean of all 
        # station locations.
        for cur_net in inv:
            for cur_station in cur_net:
                cur_loc_coord = []
                for cur_channel in cur_station:
                    cur_coords = [cur_channel.longitude,
                                  cur_channel.latitude]
                    cur_loc_coord.append(cur_coords)
                cur_mean_coord = np.mean(np.array(cur_loc_coord), axis = 0)
                cur_mean_coord = list(cur_mean_coord)
                stat_coords.append(cur_mean_coord)

        if stat_coords:
            # Use unique station coordinates only.
            #stat_coords = np.array(list(zip(lon, lat)))
            stat_coords = np.array(stat_coords)
            stat_coords = np.unique(stat_coords, axis = 0)
            lon = stat_coords[:, 0]
            lat = stat_coords[:, 1]

            # Don't create the binder grid file for only one station.
            if len(lon) <= 1:
                log.info("Binder grid file not created for only one station.")
                return

            # Compute the grid boundaries and parameters.
            round_dec = 6
            grid_limits = compute_grid_limits(lon = lon,
                                              lat = lat,
                                              round_dec = round_dec)
            
            grid_params = compute_grid_parameters(lon = lon,
                                                  lat = lat,
                                                  grid_limits = grid_limits)
            
        # Compute and export the binder grid parameters.
        eqk_dir = os.path.join(base_dir, 'eqk')
        write.write_binder_grid(eqk_dir,
                                grid_limits = grid_limits,
                                grid_params = grid_params)
