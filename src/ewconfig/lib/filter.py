from abc import ABC, abstractmethod
from fnmatch import translate
from logging import getLogger
from re import compile, split

log = getLogger(__name__)

# bit of a hack - doesn't match names and is descriptive
NONE = '<None>'


def split_globs(param):
    return split(r'[\s,]', param)


def compile_globs(param, prefix):
    '''
    Split on commas, add trailing * if prefix, and compile each separately,
    returning a list.
    '''
    globs = split_globs(param)
    if prefix:
        globs = [glob + '*' for glob in globs]
    return [compile(translate(glob)) for glob in globs]


def match_any_glob(globs, text):
    return any(glob.match(text) for glob in globs)


def first_matching_glob(texts, globs, param, sn):
    skipped = 0
    for glob in globs:
        if any(glob.match(text) for text in texts):
            patterns = split_globs(param)
            log.info(f'Will pick on channels matching the prefix {patterns[skipped]} for {sn}')
            return glob
        skipped += 1
    log.warning(f'No match in {",".join(texts)} for --pick-chan={param}')
    return None


class BaseFilter(ABC):

    @abstractmethod
    def _default_include(self):
        raise NotImplemented()

    def __init__(self, name, prefix=False, include=None, exclude=None):
        self.__name = name
        if not include:
            include = self._default_include()
        if not exclude:
            exclude = NONE
        self.__str = f'{name}: include {include}; exclude: {exclude}'
        self.__includes = compile_globs(include, prefix)
        self.__excludes = compile_globs(exclude, prefix)

    def __call__(self, items, key=None, quiet=False):
        logged = set()
        for item in items:
            if self.test(item, key=key, quiet=quiet, logged=logged):
                yield item

    def test(self, item, key=None, quiet=False, logged=None):
        if not key: key = lambda x: x
        if logged is None: logged = set()
        value = key(item)
        if match_any_glob(self.__includes, value):
            if not match_any_glob(self.__excludes, value):
                return True
            else:
                if value not in logged:
                    if not quiet:
                        log.info(f'{self.__name} {value} was explicitly excluded')
                    logged.add(value)
        else:
            if value not in logged:
                if not quiet:
                    log.info(f'{self.__name} {value} was not included')
                logged.add(value)
        return False

    def __str__(self):
        return self.__str


class GlobFilter(BaseFilter):
    """
    A filter for <name> that includes patterns in <includes>
    and then excludes patterns in <excludes>.
    If <includes> is missing then all are initially included.
    If <excludes> is missing then none are excluded.
    Patterns are 'globs' as supported by fnmatch module.
    """

    def _default_include(self):
        return '*'


class ChannelFilter(GlobFilter):

    def __init__(self, include=None, exclude=None):
        super().__init__('Channel', prefix=True, include=include, exclude=exclude)


class StationFilter(GlobFilter):

    def __init__(self, include=None, exclude=None):
        super().__init__('Station', prefix=False, include=include, exclude=exclude)


class DottedFilter(BaseFilter):
    """
    Create a filter for <name> that includes patterns in <includes>
    and then excludes patterns in <excludes>.
    If <includes> is missing then none are initially included (differs from Filter)
    If <excludes> is missing then none are excluded.
    Patterns are 'globs' as supported by fnmatch module, but unlike Filter they can contain
    multiple components separated by dots.  When matching (possibly right-incomplete) NSCLs
    the number of components must match.  (In practice these are just characters!  No special
    processing is necessary!)
    """

    def _check_dots(self, pattern):
        # this shouldn't happen when called from the command line because it's verified by the type
        n = pattern.count('.')
        if n not in (1, 3):
            log.warning(f'The pattern {pattern} does not contain 2 or 4 dotted names (N.S or N.S.C.L) so will not be used')

    def __init__(self, name, include=None, exclude=None):
        if include:
            for pattern in split_globs(include):
                self._check_dots(pattern)
        if exclude:
            for pattern in split_globs(exclude):
                self._check_dots(pattern)
        super().__init__(name, False, include, exclude)

    def _default_include(self):
        return NONE
