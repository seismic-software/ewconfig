from argparse import SUPPRESS, HelpFormatter, Action
from glob import glob
from itertools import chain
from logging import getLogger
from re import split

log = getLogger(__name__)


class ParagraphHelpFormatter(HelpFormatter):
    '''
    Preserve paragraphs in the description and epilog.
    '''

    def add_text(self, text):
        if text is not SUPPRESS and text is not None:
            for para in split(r'\n\s*\n', text):
                self._add_item(self._format_text, [para])


def add_version_args(parser):
    from .. import __version__
    parser.add_argument('-V', '--version', action='version', version=__version__,
                        help='Show program\'s version number and exit.')


class GlobExpansion(Action):
    '''
    Glob expand file names.   Gives automatic globbing on windows (shell does the
    work on unix and this has no effect).
    '''

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        # list flattening
        expanded = list(chain(*[glob(value) for value in values]))
        # this looks ugly because logs haven't been configured yet
        # if expanded != values:
        #     log.warning(f'Globbed {",".join(values)} into {",".join(expanded)}')
        namespace.__setattr__(self.dest, expanded)
