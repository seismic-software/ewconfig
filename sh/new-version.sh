#!/bin/bash

cd "${BASH_SOURCE%/*}/" || exit
cd ..
source env/bin/activate

OLD_VERSION=`stationxml2ew --version`

echo "old version $OLD_VERSION"

if [ $# -ne 1 ]; then
    echo "provide new version as single argument"
    exit 1
fi

NEW_VERSION=$1
echo "new version $NEW_VERSION"

BRANCH=`git rev-parse --abbrev-ref HEAD`
if [ "$BRANCH" != "dev" ]; then
    echo "set new version on dev branch"
    exit 2
fi

rm dist/ewconfig-$OLD_VERSION*
sh/create-doc.sh

sed -i -e s/$OLD_VERSION/$NEW_VERSION/ src/ewconfig/__init__.py
sed -i -e s/$OLD_VERSION/$NEW_VERSION/ pyproject.toml
grep -H version src/ewconfig/__init__.py
grep -H version pyproject.toml

git commit -am "version $NEW_VERSION"
git checkout master
git merge dev
git tag -a "v$NEW_VERSION" -m "version $NEW_VERSION"

echo "WARNING: you are now on master and nothing has been pushed"
echo
echo "git push"
echo "git push --tags"
echo "git checkout dev"
echo "git push"

