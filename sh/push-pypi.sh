#!/bin/bash

PYTHON=python3.9
REPO=testpypi

BRANCH=`git rev-parse --abbrev-ref HEAD`
if [ "$BRANCH" != "master" ]; then
    echo "deploy from master branch"
    exit 1
fi

# if you're doing this for real then provide pypi as the only argument
# but first try it on testpypi!
if [ $# == 1 ]; then
    REPO=$1
fi

echo "uploading to $REPO"
sleep 5

cd "${BASH_SOURCE%/*}/" || exit
cd ..

source env/bin/activate

python -m pip install --upgrade pip build twine 
python -m build
# this is going to prompt for your token, so you need a token...
python -m twine upload --repository $REPO dist/*

