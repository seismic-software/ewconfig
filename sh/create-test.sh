#!/bin/bash

cd "${BASH_SOURCE%/*}/" || exit
cd ..
ROOT=`pwd`
source env/bin/activate

cd $ROOT/data
rm -fr ci-pasc
mkdir ci-pasc
cd ci-pasc
wget -O CI-PASC.xml 'http://service.iris.edu/fdsnws/station/1/query?network=CI&station=PASC&level=response&start=2020-09-01T00:00:00'
stationxml-seed-converter CI-PASC.xml --output CI-PASC.dataless
mkdir test
cd test
rdseed -pf ../CI-PASC.dataless
sacpz2ew .
