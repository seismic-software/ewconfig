#!/bin/bash

cd "${BASH_SOURCE%/*}/" || exit
cd ..
source env/bin/activate

stationxml2ew --md-help > doc/stationxml2ew.md
sacpz2ew --md-help > doc/sacpz2ew.md
ewmerge --md-help > doc/ewmerge.md
ewprune --md-help > doc/ewprune.md
