#                                      threshold1
# Pick  Pin     Sta/Comp           longTermWindow  tUpEvent
# Flag  Numb    Net/Loc       filterWindow  threshold2
# ----  ----    --------      -----------------------------
1    1 INC01 HHE X2 -- -1 -1 10.0 10.0 -1
1    1 INC01 HHN X2 -- -1 -1 10.0 10.0 -1
1    1 INC01 HHZ X2 -- -1 -1 10.0 10.0 -1
1    1 INC02 HHE X2 -- -1 -1 10.0 10.0 -1
1    1 INC02 HHN X2 -- -1 -1 10.0 10.0 -1
1    1 INC02 HHZ X2 -- -1 -1 10.0 10.0 -1
1    1 INC03 HHE X2 -- -1 -1 10.0 10.0 -1
1    1 INC03 HHN X2 -- -1 -1 10.0 10.0 -1
1    1 INC03 HHZ X2 -- -1 -1 10.0 10.0 -1
1    1 INC04 HHE X2 -- -1 -1 10.0 10.0 -1
1    1 INC04 HHN X2 -- -1 -1 10.0 10.0 -1
1    1 INC04 HHZ X2 -- -1 -1 10.0 10.0 -1
1    1 INC05 HHE X2 -- -1 -1 10.0 10.0 -1
1    1 INC05 HHN X2 -- -1 -1 10.0 10.0 -1
1    1 INC05 HHZ X2 -- -1 -1 10.0 10.0 -1
1    1 INC06 HHE X2 -- -1 -1 10.0 10.0 -1
1    1 INC06 HHN X2 -- -1 -1 10.0 10.0 -1
1    1 INC06 HHZ X2 -- -1 -1 10.0 10.0 -1
1    1 INC07 HHE X2 -- -1 -1 10.0 10.0 -1
1    1 INC07 HHN X2 -- -1 -1 10.0 10.0 -1
1    1 INC07 HHZ X2 -- -1 -1 10.0 10.0 -1
1    1 INC08 HHE X2 -- -1 -1 10.0 10.0 -1
1    1 INC08 HHN X2 -- -1 -1 10.0 10.0 -1
1    1 INC08 HHZ X2 -- -1 -1 10.0 10.0 -1
1    1 R50A HH1 N4 00 -1 -1 10.0 10.0 -1
1    1 R50A HH2 N4 00 -1 -1 10.0 10.0 -1
1    1 R50A HHZ N4 00 -1 -1 10.0 10.0 -1
1    1 PASC HHE CI 00 -1 -1 10.0 10.0 -1
1    1 PASC HHE CI 10 -1 -1 10.0 10.0 -1
1    1 PASC HHN CI 00 -1 -1 10.0 10.0 -1
1    1 PASC HHN CI 10 -1 -1 10.0 10.0 -1
1    1 PASC HHZ CI 00 -1 -1 10.0 10.0 -1
1    1 PASC HHZ CI 10 -1 -1 10.0 10.0 -1
