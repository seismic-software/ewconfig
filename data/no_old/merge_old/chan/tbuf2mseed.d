Send_scnl PASC ACE CI 00 10
Send_scnl PASC ACE CI 10 10
Send_scnl PASC BCI CI 00 10
Send_scnl PASC BHE CI 00 10
Send_scnl PASC BHE CI 10 10
Send_scnl PASC BHN CI 00 10
Send_scnl PASC BHN CI 10 10
Send_scnl PASC BHZ CI 00 10
Send_scnl PASC BHZ CI 10 10
Send_scnl PASC HCI CI 00 10
Send_scnl PASC HHE CI 00 10
Send_scnl PASC HHE CI 10 10
Send_scnl PASC HHN CI 00 10
Send_scnl PASC HHN CI 10 10
Send_scnl PASC HHZ CI 00 10
Send_scnl PASC HHZ CI 10 10
Send_scnl PASC HNE CI 00 10
Send_scnl PASC HNE CI 10 10
Send_scnl PASC HNN CI 00 10
Send_scnl PASC HNN CI 10 10
Send_scnl PASC HNZ CI 00 10
Send_scnl PASC HNZ CI 10 10
Send_scnl PASC LCE CI 00 10
Send_scnl PASC LCE CI 10 10
Send_scnl PASC LCL CI 00 10
Send_scnl PASC LCL CI 10 10
Send_scnl PASC LCQ CI 00 10
Send_scnl PASC LCQ CI 10 10
Send_scnl PASC LDE CI 00 10
Send_scnl PASC LDN CI 00 10
Send_scnl PASC LDZ CI 00 10
Send_scnl PASC LHE CI 00 10
Send_scnl PASC LHE CI 10 10
Send_scnl PASC LHN CI 00 10
Send_scnl PASC LHN CI 10 10
Send_scnl PASC LHZ CI 00 10
Send_scnl PASC LHZ CI 10 10
Send_scnl PASC LNE CI 00 10
Send_scnl PASC LNE CI 10 10
Send_scnl PASC LNN CI 00 10
Send_scnl PASC LNN CI 10 10
Send_scnl PASC LNZ CI 00 10
Send_scnl PASC LNZ CI 10 10
Send_scnl PASC LOG CI 00 10
Send_scnl PASC LOG CI 10 10
Send_scnl PASC OCF CI 00 10
Send_scnl PASC OCF CI 10 10
Send_scnl PASC VCE CI 00 10
Send_scnl PASC VCE CI 10 10
Send_scnl PASC VCO CI 00 10
Send_scnl PASC VCO CI 10 10
Send_scnl PASC VCQ CI 00 10
Send_scnl PASC VCQ CI 10 10
Send_scnl PASC VDO CI 00 10
Send_scnl PASC VEA CI 00 10
Send_scnl PASC VEA CI 10 10
Send_scnl PASC VEC CI 00 10
Send_scnl PASC VEC CI 10 10
Send_scnl PASC VEP CI 00 10
Send_scnl PASC VEP CI 10 10
Send_scnl PASC VFP CI 00 10
Send_scnl PASC VFP CI 10 10
Send_scnl PASC VHE CI 00 10
Send_scnl PASC VHE CI 10 10
Send_scnl PASC VHN CI 00 10
Send_scnl PASC VHN CI 10 10
Send_scnl PASC VHZ CI 00 10
Send_scnl PASC VHZ CI 10 10
Send_scnl PASC VKI CI 00 10
Send_scnl PASC VKI CI 10 10
Send_scnl PASC VKO CI 00 10
Send_scnl PASC VME CI 00 10
Send_scnl PASC VMN CI 00 10
Send_scnl PASC VMU CI 10 10
Send_scnl PASC VMV CI 10 10
Send_scnl PASC VMW CI 10 10
Send_scnl PASC VMZ CI 00 10
Send_scnl R50A HH1 N4 00 10
Send_scnl R50A HH2 N4 00 10
Send_scnl R50A HHZ N4 00 10
Send_scnl INC01 HHE X2 -- 10
Send_scnl INC01 HHN X2 -- 10
Send_scnl INC01 HHZ X2 -- 10
Send_scnl INC02 HHE X2 -- 10
Send_scnl INC02 HHN X2 -- 10
Send_scnl INC02 HHZ X2 -- 10
Send_scnl INC03 HHE X2 -- 10
Send_scnl INC03 HHN X2 -- 10
Send_scnl INC03 HHZ X2 -- 10
Send_scnl INC04 HHE X2 -- 10
Send_scnl INC04 HHN X2 -- 10
Send_scnl INC04 HHZ X2 -- 10
Send_scnl INC05 HHE X2 -- 10
Send_scnl INC05 HHN X2 -- 10
Send_scnl INC05 HHZ X2 -- 10
Send_scnl INC06 HHE X2 -- 10
Send_scnl INC06 HHN X2 -- 10
Send_scnl INC06 HHZ X2 -- 10
Send_scnl INC07 HHE X2 -- 10
Send_scnl INC07 HHN X2 -- 10
Send_scnl INC07 HHZ X2 -- 10
Send_scnl INC08 HHE X2 -- 10
Send_scnl INC08 HHN X2 -- 10
Send_scnl INC08 HHZ X2 -- 10
