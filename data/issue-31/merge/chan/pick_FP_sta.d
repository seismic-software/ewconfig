#                                      threshold1
# Pick  Pin     Sta/Comp           longTermWindow  tUpEvent
# Flag  Numb    Net/Loc       filterWindow  threshold2
# ----  ----    --------      -----------------------------
1    1 DSFO HHE OH 10 -1 -1 10.0 10.0 -1
1    1 DSFO HHN OH 10 -1 -1 10.0 10.0 -1
1    1 DSFO HHZ OH 10 -1 -1 10.0 10.0 -1
# 2023-11-14 This is a comment! Added by ewmerge, part of ewconfig.
#                                      threshold1
# Pick  Pin     Sta/Comp           longTermWindow  tUpEvent
# Flag  Numb    Net/Loc       filterWindow  threshold2
# ----  ----    --------      -----------------------------
1    1 HINO HHE OH 10 -1 -1 10.0 10.0 -1
1    1 HINO HHN OH 10 -1 -1 10.0 10.0 -1
1    1 HINO HHZ OH 10 -1 -1 10.0 10.0 -1
#                                      threshold1
# Pick  Pin     Sta/Comp           longTermWindow  tUpEvent
# Flag  Numb    Net/Loc       filterWindow  threshold2
# ----  ----    --------      -----------------------------
1    1 KIOH HHE OH 10 -1 -1 10.0 10.0 -1
1    1 KIOH HHN OH 10 -1 -1 10.0 10.0 -1
1    1 KIOH HHZ OH 10 -1 -1 10.0 10.0 -1
