#
#                     Configuration File for slink2ew
#
MyModuleId       MOD_SLINK2EW
RingName         WAVE_RING       # Transport ring to write output to.
#
HeartBeatInterval     30         # Heartbeat interval, in seconds.
LogFile               1          # 1 -> Keep log, 0 -> no log file
                                 # 2 -> write to module log but not stderr/stdout
#Verbosity      0                # Set level of verbosity.
#
SLhost     st27.gfz-potsdam.de   # Host address of the SeedLink server
SLport         18000             # Port number of the SeedLink server
#
StateFile      slink2ew.GE       # File with a list of sequence numbers which
                                 # is read during module startup to resume
                                 # data streams.  This file is written on a
                                 # clean module shutdown.  If this is not
                                 # specified as an absolute path the value of
                                 # the environment variable EW_PARAMS will
                                 # be pre-pended to this value.  Using this
                                 # functionality is highly recommended.
#
#NetworkTimeout 600              # Network timeout, after this many idle
                                 # seconds the connection will be reset.
                                 # Default is 600 seconds, 0 to disable.
#
#NetworkDelay   30               # Network re-connect delay in seconds.
#
#KeepAlive      0                # Send keepalive packets (when idle) at this
                                 # interval in seconds.  Default is 0 (disabled).
#
#ForceTraceBuf1 0                # On systems that support TRACEBUF2
                                 # messages this flag will force the module
                                 # to create TRACEBUF messages instead.
                                 # Most people will never need this.
#
# Selectors and Stream's.  If any Stream lines are specified the connection
# to the SeedLink server will be configured in multi-station mode using
# Selectors, if any, as defaults.  If no Stream lines are specified the
# connection will be configured in uni-station mode using Selectors, if any.
#
Selectors      "BH?.D"           # SeedLink selectors.  These selectors are used
                                 # for a uni-station mode connection.  If one
                                 # or more 'Stream' entries are given these are
                                 # used as default selectors for multi-station
                                 # mode data streams.  See description of
                                 # SeedLink selectors below.  Multiple selectors
                                 # must be enclosed in quotes.
#
#
# List each data stream (a network and station code pair) that you
# wish to request from the server with a "Stream" command.  If one or
# more Stream commands are given the connection will be configured in
# multi-station mode (multiple station data streams over a single
# network connection).  If no Stream commands are specified the
# connection will be configured in uni-station mode, optionally using
# any specified "Selectors".  A Stream command should be followed by a
# stream key, a network code followed by a station code separated by
# an underscore (i.e. IU_KONO).  SeedLink selectors for a specific
# stream may optionally be specified after the stream key.  Multiple
# selectors must be enclosed in quotes.  Any selectors specified with
# the Selectors command above are used as defaults when no selectors
# are specified for a given stream.
#
# Combined with the above specified default selectors the BHx channels
# will be requested for each of these stations except for DSB for which
# we also want the LHx channels.
#
Stream  GE_DSB   "BH?.D LH?.D"
Stream  GE_ISP   CONFLICT
Stream  GE_APE
Stream  GE_STU
#
#
# Some SeedLink servers support extended selection capability allowing
# wildcards (either '*' or '?') in the network and station fields, for example
# to select all stations from the TA network:
#
Stream TA_*
