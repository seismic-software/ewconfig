#                                      threshold1
# Pick  Pin     Sta/Comp           longTermWindow  tUpEvent
# Flag  Numb    Net/Loc       filterWindow  threshold2
# ----  ----    --------      -----------------------------
1    1 NMP11 HHE GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP11 HHN GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP11 HHZ GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP11 HNE GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP11 HNN GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP11 HNZ GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP12 HHE GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP12 HHN GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP12 HHZ GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP12 HNE GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP12 HNN GM 01 2.4 6.0 10.0 8.0 0.2
1    1 NMP12 HNZ GM 01 2.4 6.0 10.0 8.0 0.2
0    1 TX32 BHE IM -- CONFLICT
