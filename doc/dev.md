
# Development

Hi, this document is written by Andrew Cooke (andrewcooke@isti.com).

I seem to take most of the responsibility for this code (like, get
called into meetings and asked to explain it), even though a bunch of
people have contributed / continue to contribute.  So please, if
you're contributing / modifying the code:

* Introduce yourself and explain what is happening (I probably won't
  be told otherwise).

* Try to respect the way that the code works currently.

* Make sure that all tests pass before committing (`sh/run-tests.sh`).

* Work on the dev branch (or a private branch that you merge to dev)
  and then, once done, use `sh/new-version.sh` to "publish" your work
  to master as a new version:

  * Generally bump the minor version unless the interface changes (eg
    parameter names change) or we're generating new files (ie things
    that people who call the code need to respond to).

  * Announce the new version in the ewconfig chat.

* If you're unsure about something, please ask.  I'm happy to help
  refactor stuff to make extension easier.

Cheers!

