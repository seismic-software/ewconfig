# ewprune:
```
usage: ewprune [--delete PATTERN [PATTERN ...]] [--keep PATTERN [PATTERN ...]]
               [--force] [--pruned PRUNED] [--backup BACKUP] [-h] [-V]
               [--md-help] [-v INTEGER]
               DIR

```
A tool remove specific sources from an EW configuration.

To do this, sources are matched against patterns. Patterns are dotted
components that can contain \*, ? (wildcards), [...] and [!...] (alternatives).
Patterns only match NSCLs if the number of components matches. So the pattern
'\*.\*.HN[NE].\*' will match CI.PASC.HNN.10 for example, but will not match
CI.PASC (without channel or location). Multiple patterns can be specified and
sources are only deleted if they match a pattern in `--delete` and do not
match a pattern in `--keep`.

This may seem cumbersome, but seems to be the only approach that gives
complete control.

By default the sources to be deleted are displayed (only). To actually force
deletion you must add `--force`.

If --pruned is given, the new, pruned config is placed in that directory.

If --pruned is not given, the pruned config replaces the contents of the --dir
directory. In this case --backup (which will contain the original contents of
--dir) must be given. If you really don't want a backup use "--backup NONE".

## Examples

To delete all mention of the CI.PASC station (both N.S and N.S.C.L entries in
various files):

ewprune DIR --delete CI.PASC CI.PASC.\*.\* --backup ...

To remove all HHZ channels

ewprune DIR --delete \*.\*.HHZ.\* --backup ...

Note that in the example above N.S entries will remain, so if station NET.XXX
had only HHZ then that must also be explicitly deleted:

ewprune DIR --delete \*.\*.HHZ.\* NET.XXX --backup ...

Alternatively, maybe we know that only NET.YYY had alternative channels, so we
can delete all N.S entries except that:

ewprune DIR --delete \*.\*.HHZ.\* \*.\* --keep NET.YYY --backup ...

## positional arguments:
### DIR
```
Directory containing the configuration
```

## optional arguments:
### --delete PATTERN [PATTERN ...]
```
Patterns to delete
```

### --keep PATTERN [PATTERN ...]
```
Patterns to keep
```

### --force
```
Force deletion
```

### --pruned PRUNED
```
Destination directory for pruned config.
```

### --backup BACKUP
```
Backup directory for original config (if no --pruned; can be NONE).
```

### -h
```
Show this help message and exit.
```

### -V, --version
```
Show program's version number and exit.
```

### --md-help
```
Print Markdown-formatted help text and exit.
```

### -v INTEGER, --verbosity INTEGER
```
Log level (0: silent, 5: debug)
```
