# ewmerge:
```
usage: ewmerge [--old OLD] --new NEW [NEW ...] [--prefer-old | --prefer-new]
               [--merged MERGED] [--backup BACKUP] [--sort SORT]
               [--comment [COMMENT]] [--all-chan] [-h] [-V] [--md-help]
               [-v INTEGER] [--xml-include-chan GLOB]
               [--xml-exclude-chan GLOB] [--xml-include-sta GLOB]
               [--xml-exclude-sta GLOB] [--date DATE] [--pick-fp-sta STRING]
               [--thresh FLOAT] [--displacement | --velocity | --acceleration]
               [-p GLOB] [-n] [-g]

```
A program to merge EW configurations (at --old and --new) selecting entries
based on NSCLs.

Channel selection is described in more detail in the help for stationxml2ew
(stationxml2ew -h).

The --new sources can be Station XML format files or EW config directories.

In the final output, files in the eqk/response and chan sub-directories are
merged / filtered from all directories and files. All other configuration data
are taken from --old (if given).

If --merged is given, the new, merged config is placed in that directory.

If --merged is not given, but --old is used, the merged config replaces the
current contents of the --old directory. In this case --backup (which will
contain the original contents of --old) must be given. If you really don't
want a backup use "--backup NONE".

## Examples

To merge data from all the station xml files in the "input" directory, placing
the EW config in the "output" directory:

ewmerge --new input/\*.xml --merged output

To merge data from all the station xml files in the "input" directory into an
existing "config" directory:

ewmerge --new input/\*.xml --old config --backup old-config

To merge data from all the station xml files in the "input" directory with an
existing "config" directory, placing the results in a new "output" directory:

ewmerge --new input/\*.xml --old config --merged output

## Merge parameters:
### --old OLD
```
Directory for old (full) config.
```

### --new NEW [NEW ...]
```
Directory(s) or XML file(s) for new (chan and response) config.  XML files are expanded by stationxml2ew (see parameters below).
```

### --prefer-old
```
If a NSCL is present in old and new, use the old entries (default is to require identical entries).
```

### --prefer-new
```
If a NSCL is present in old and new, use the new entries.
```

### --merged MERGED
```
Destination directory for merged config.
```

### --backup BACKUP
```
Backup directory for old config (if --old and no --merged; can be NONE).
```

### --sort SORT
```
Sorting order for output (eg SNCL, NS).
```

### --comment [COMMENT]
```
Comment before second file (no argument for timestamp only)
```

### --all-chan
```
Fail if a file in the chan directory is an unknown type (default is to ignore).
```

### -h
```
Show this help message and exit.
```

### -V, --version
```
Show program's version number and exit.
```

### --md-help
```
Print Markdown-formatted help text and exit.
```

### -v INTEGER, --verbosity INTEGER
```
Log level (0: silent, 5: debug)
```

## XML expansion parameters (passed to stationxml2ew):
### --xml-include-chan GLOB
```
Globs to include channel name (prefix, default all).
```

### --xml-exclude-chan GLOB
```
Globs to exclude channel name (prefix, default none).
```

### --xml-include-sta GLOB
```
Globs to include station names (full match, default all).
```

### --xml-exclude-sta GLOB
```
Globs to exclude station names (full match, default none).
```

### --date DATE
```
Use channels valid on this date (default now).
```

### --pick-fp-sta STRING
```
Extra values for pick_FP_sta.d (default "-1 -1 10.0 10.0 -1").
```

### --thresh FLOAT
```
Threshold value for thresh_channels.d (default 21000.00)
```

### --displacement
```
Assume unrecognized responses are displacement.
```

### --velocity
```
Assume unrecognized responses are velocity.
```

### --acceleration
```
Assume unrecognized responses are acceleration.
```

### -p GLOB, --pick-chan GLOB
```
Prefixes for picked channels (globs in priority order, default CH,HH,BH,DH,EH,SH,CN,HN,BN,EN,HJ)
```

### -n, --nano
```
Convert Constant meters into nanometers. You must use this option if you are using the default Localmag or GMEW settings. If you have enabled the ResponseInMeters option in localmag.d and/or gmew.d, then you don't need this option.
```

### -g, --geophone
```
Treat ALL instruments as Geophones in pick_ew output. Otherwise broadband settings are used unless channel starts with E.
```
