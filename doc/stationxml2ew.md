# stationxml2ew:
```
usage: stationxml2ew [-h] [--dir [DIR ...]] [--xml-out FILE [FILE ...]]
                     [--xml-include-chan GLOB] [--xml-exclude-chan GLOB]
                     [--xml-include-sta GLOB] [--xml-exclude-sta GLOB]
                     [--date DATE] [--pick-fp-sta STRING] [--thresh FLOAT]
                     [--displacement | --velocity | --acceleration] [-p GLOB]
                     [-n] [-g] [-V] [--md-help] [-v INTEGER]
                     FILE [FILE ...]

```
A scanner program to convert a station.xml file to configurations suitable for
use with localmag, hypoinverse, pick_ew, pick_FP, slink2ew, carlstatrig and
wave_serverV station lists. It is presumed that the input file is complete,
with lat/long/elevation for the SCNL. Output sac files will be in meters
unless the --nano parameter is set to convert to nanometers.

When reading data from XML files, stations and channels can be selected using
--xml-include-chan (-sta) and --xml-exclude-chan (-sta). These take globs that
can match any string (\*), any character (?), and any character from a sequence
([abc]). If --xml-include-chan (-sta) is not given it is assumed to be "\*".
Channels, for example, are included if they match --xml-include-chan and do
not match --xml-exclude-chan. Channel matching requires only a prefix, but
stations require a full match.

Specifying --date selects channels active at that time. By default this is the
current time.

The --pick-chan parameter provides additional filtering for selected files.
This is an ordered list of channel prefixes. The first prefix that matches any
channel read from the XML is used to filter all channels. So, for example, if
--pick-chan is CH,HH and the XML file contains CH, HH and EH channels, only
the CH channels will be used, but if the XML file contains HH and EH channels
(no CH channels) then HH channels will be used. If you want to include both CH
and HH channels, then specify a single glob that will match both, like [CH]H.

Responses types are recognised via the response units. Unrecognised units can
be given a default using --displacement|--velocity|--acceleration. If no
default is given they are skipped. Pressure sensitive stations (units Pa) are
always skipped.

## Examples

To generate a config in the current directory from an XML file:

stationxml2ew station.xml

To generate a config in a different directory:

stationxml2ew station.xml --dir output/dir

To process multiple XML files, each with a different output directory:

stationxml2ew station1.xml station2.xml --dir station1 station2

To convert multiple XML files into a single directory see ewmerge (which can
call this program and then do the merge to a single directory).

## positional arguments:
### FILE
```
Station XML file(s)
```

## optional arguments:
### -h, --help
```
show this help message and exit
```

### --dir [DIR ...]
```
Output directory(s) (default CWD for a single input file)
```

### --xml-out FILE [FILE ...]
```
Filtered station.xml output
```

### --xml-include-chan GLOB
```
Globs to include channel name (prefix, default all).
```

### --xml-exclude-chan GLOB
```
Globs to exclude channel name (prefix, default none).
```

### --xml-include-sta GLOB
```
Globs to include station names (full match, default all).
```

### --xml-exclude-sta GLOB
```
Globs to exclude station names (full match, default none).
```

### --date DATE
```
Use channels valid on this date (default now).
```

### --pick-fp-sta STRING
```
Extra values for pick_FP_sta.d (default "-1 -1 10.0 10.0 -1").
```

### --thresh FLOAT
```
Threshold value for thresh_channels.d (default 21000.00)
```

### --displacement
```
Assume unrecognized responses are displacement.
```

### --velocity
```
Assume unrecognized responses are velocity.
```

### --acceleration
```
Assume unrecognized responses are acceleration.
```

### -p GLOB, --pick-chan GLOB
```
Prefixes for picked channels (globs in priority order, default CH,HH,BH,DH,EH,SH,CN,HN,BN,EN,HJ)
```

### -n, --nano
```
Convert Constant meters into nanometers. You must use this option if you are using the default Localmag or GMEW settings. If you have enabled the ResponseInMeters option in localmag.d and/or gmew.d, then you don't need this option.
```

### -g, --geophone
```
Treat ALL instruments as Geophones in pick_ew output. Otherwise broadband settings are used unless channel starts with E.
```

### -V, --version
```
Show program's version number and exit.
```

### --md-help
```
Print Markdown-formatted help text and exit.
```

### -v INTEGER, --verbosity INTEGER
```
Log level (0: silent, 5: debug)
```
