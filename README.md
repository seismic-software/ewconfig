
# ewconfig

* Generate Earthworm configuration from Station XML
  ([stationxml2ew](doc/stationxml2ew.md)) or SEED files (rdseed and
  [sacpz2ew](doc/sacpz2ew.md)).

* Merge data from different configurations ([ewmerge](doc/ewmerge.md)).

* Remove sources from a configuration ([ewprune](doc/ewprune.md)).

## Further Documentation

* [Install](INSTALL.md)

* [Examples](EXAMPLES.md)

